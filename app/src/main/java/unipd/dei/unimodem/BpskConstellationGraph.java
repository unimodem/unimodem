package unipd.dei.unimodem;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;



public class BpskConstellationGraph extends AppCompatActivity{
    private final String TAG = "BPSK_GRAPH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph_layout);
        Intent intent = getIntent();
        double[] valuesSin = intent.getDoubleArrayExtra("constellationSin");
        double[] valuesCos = intent.getDoubleArrayExtra("constellationCos");

        Log.d(TAG, "onCreate: ricevuti "+valuesSin.length+" valori su sin per il grafico");
        Log.d(TAG, "onCreate: ricevuti "+valuesCos.length+" valori su cos per il grafico");

        //calcola il valor medio dei moduli dei vettori ricevuti
        double media = 0;
        for (int i = 0; i < valuesSin.length; i++) {
            media += Math.sqrt(valuesSin[i]*valuesSin[i]+valuesCos[i]*valuesCos[i]);
        }
        media /= valuesSin.length;
        //normalizza i vettori sul valore medio
        for(int i = 0; i < valuesSin.length; i++) {
            valuesSin[i] /= media;
            valuesCos[i] /= media;
        }

        GraphView graph = (GraphView) findViewById(R.id.graph);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(-3);
        graph.getViewport().setMaxX(3);

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(-3);
        graph.getViewport().setMaxY(3);

        int ref = 1;//utilizzo come riferimento solamente il primo bit
        //creo una serie con tutti i valori succssivi al primo (che è invece il riferimento)
        DataPoint[] datapoint = new DataPoint[valuesSin.length-ref];
        for (int i = 0; i < valuesSin.length-ref; i++) {
            datapoint[i] = new DataPoint(valuesSin[i+ref], valuesCos[i+ref]);
            //Log.d(TAG, "onCreate: value:"+valuesSin[i]+"index:"+i);
        }
        PointsGraphSeries<DataPoint> constellation = new PointsGraphSeries<>(datapoint);
        constellation.setSize(10);
        graph.addSeries(constellation);

            //creo una serie di riferimento con i primi punti
        datapoint = new DataPoint[ref];
        for (int i = 0; i < ref; i++) {
            datapoint[i] = new DataPoint(valuesSin[i], valuesCos[i]);
        }
        PointsGraphSeries<DataPoint> idealConstellation = new PointsGraphSeries<>(datapoint);
        idealConstellation.setColor(Color.GREEN);
        graph.addSeries(idealConstellation);
    }
}