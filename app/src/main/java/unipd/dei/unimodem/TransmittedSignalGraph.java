package unipd.dei.unimodem;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class TransmittedSignalGraph extends AppCompatActivity {
    private final static String TAG = "TRANSMIT_SIGNAL_GRAPH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graphsignal_layout);
        Log.e(TAG, "onCreate: grafico segnale trasmesso");
        Intent intent = getIntent();
        short[] signal = intent.getShortArrayExtra("signal");
        GraphView graph = (GraphView) findViewById(R.id.signalgraph);

        graph.getViewport().setScalable(true);
        graph.getViewport().setScrollable(true);

        // activate vertical scrolling
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(2000);
        DataPoint[] datapoint = new DataPoint[signal.length];
        for(int i = 0; i < signal.length; i++) datapoint[i] = new DataPoint(i, signal[i]);
        LineGraphSeries<DataPoint> signalSeries = new LineGraphSeries<>(datapoint);
        graph.addSeries(signalSeries);
    }
}