package unipd.dei.unimodem;

import android.util.Log;

class Bpsk implements Modem {
    //tag per il debug
    private final String TAG = "BPSK";

    private int sampleRate;
    private int carrierFrequency;
    private int periodsForSymbol;
    private int samplesForSymbol;


    private double[] demodulatedSignalConstellationSin;
    private double[] demodulatedSignalConstellationCos;

    /**
     * Bpsk
     * @param sampleRate freqeunza di campionamento del segnale
     * @param carrierFrequency frequenza della modulante
     * @param periodsForSymbol numero di periodi della modulante da trasmettere per ogni simbolo
     */
    Bpsk(int sampleRate, int carrierFrequency, int periodsForSymbol){
        if(sampleRate>0){
            this.sampleRate = sampleRate;
        } else {
            Log.e(TAG, "imposibile creare il modulatore! Parametri non validi");
            throw new IllegalArgumentException();
        }
        if(carrierFrequency >0){
            this.carrierFrequency = carrierFrequency;
        } else {
            Log.e(TAG, "imposibile creare il modulatore! Parametri non validi");
            throw new IllegalArgumentException();
        }
        if(periodsForSymbol >0) {
            this.periodsForSymbol = periodsForSymbol;
        } else {
            Log.e(TAG, "imposibile creare il modulatore! Parametri non validi");
            throw new IllegalArgumentException();
        }
        this.samplesForSymbol = (int)Math.ceil((double)this.sampleRate /(double)this.carrierFrequency *(double)this.periodsForSymbol);
        Log.d(TAG,"lunghezza dei simboli: "+ samplesForSymbol);
    }
    /**
     * demodulazione
     * @param buffer array di byte che deve essere trasmesso
     * @param length lunghezza dell'array da trasmettere
     * @return segnale modulato, immagazzinato in un array di short[]
     */
    public short[] modulate(byte[] buffer, int length){
        Log.d(TAG,"inizio Modulazione BPSK");
        int samplesForSignal = samplesForSymbol *length*8;//calcolo la lunghezza dell'array che dovrà ospitare il segnale modulato
        short[] modulatedSignal = SignalProcessing.sin(carrierFrequency,samplesForSignal, sampleRate);
        int nextSample = 0;
        for (int i = 0; i < length;i++) {
            boolean bits[] = SignalProcessing.convertToBits(buffer[i]);
            for(int j=0;j<bits.length;j++){
                if(!bits[j]) {
                    //questo codice implementa il filtro htx, con con onda quadra
                    for (int k = 0; k < samplesForSymbol; k++) {
                        modulatedSignal[nextSample++] *= -1;
                    }
                }
                else{
                    nextSample += samplesForSymbol;
                }
            }
        }
        Log.d(TAG,"fine Modulazione BPSK");
        return modulatedSignal;
    }

    /**
     * demodulazione
     * @param buffer segnale ricevuto
     * @param length lunghezza del segnale
     * @param pointer puntatore all'inizio del segnale effettivo, escluso il preambolo
     * @return array di byte contentente il segnale demodulato, a partire dal pointer
     */
    public byte[] demodulate(short[] buffer, int length, int pointer){
        int signalLength = length-pointer;
        int codeLength = (int)Math.ceil((double)signalLength/(double) samplesForSymbol);//arrotondo la lunghezza per ECCESSO! è possibile che si decodifichi un bit in più rispetto a quelli realmente ricevuti
        byte value = 0;
        byte[] result = new byte[codeLength];
        demodulatedSignalConstellationSin = new double[codeLength];
        demodulatedSignalConstellationCos = new double[codeLength];
        short[] sineWave = SignalProcessing.sin(carrierFrequency,signalLength, sampleRate);
        short[] cosWave = SignalProcessing.cos(carrierFrequency,signalLength, sampleRate);
        double sumSin = 0;
        double sumCos = 0;
        double firstProjectionOnSin = 0;
        double firstProjectionOnCos = 0;
        int k = 0;//contatore dei campioni per ogni bit
        int j = 0;//contatore dei bit
        int l = 0;//contatore dei bit per ogni byte
        int m = 0;//contatore dei byte
        int lowerBound =(int)Math.floor(samplesForSymbol/10);
        int upperBound = (int)Math.ceil(samplesForSymbol*9/10);
        //calcolo lo sfasamento
        for (int i = pointer+lowerBound; i < pointer+upperBound; i++) {
            firstProjectionOnSin += buffer[i] * sineWave[i - pointer];
            firstProjectionOnCos += buffer[i] * cosWave[i - pointer];
        }
        double cosRotationAngle = firstProjectionOnSin/(Math.sqrt(firstProjectionOnCos*firstProjectionOnCos+firstProjectionOnSin*firstProjectionOnSin));
        double sinRotationAngle = firstProjectionOnCos/(Math.sqrt(firstProjectionOnCos*firstProjectionOnCos+firstProjectionOnSin*firstProjectionOnSin));

        Log.d(TAG,"inizio ciclo demodulazione");
        for(int i = pointer; i < length; i++) {
            if(k>lowerBound && k<upperBound) {
                sumSin += buffer[i] * sineWave[i - pointer];
                sumCos += buffer[i] * cosWave[i - pointer];
            }
            k++;
            if(k >= samplesForSymbol){
                if(sumSin*cosRotationAngle < 0){
                    value += Math.pow(2, 7 - l);
                }
                demodulatedSignalConstellationSin[j] = sumSin;
                demodulatedSignalConstellationCos[j] = sumCos;
                j++;
                sumSin = 0;
                sumCos = 0;
                k = 0;
                if(l++ >= 7){
                    result[m++] = value;
                    value = 0;
                    l = 0;
                }
            }
        }
        //se alla fine del ciclo non si è giunti alla conclusione del simbolo vengono effettuate le operazioni con il pezzo di simbolo presente
        if(k>0){
            if(sumSin*cosRotationAngle < 0){
                value += Math.pow(2, 7 - l);
            }
            demodulatedSignalConstellationSin[j] = sumSin;
            demodulatedSignalConstellationCos[j] = sumCos;
            if(l >= 7){
                result[m] = value;
            }
        }
        Log.d(TAG,"fine ciclo demodulazione");
        return result;
    }

    /**
     * getCostellation
     * @return restituisce la costellazione
     */
    public double[][] getCostellation() {
        double[][] result = new double[2][];
        result[0] = demodulatedSignalConstellationSin;
        result[1] = demodulatedSignalConstellationCos;
        return result;
    }
}