package unipd.dei.unimodem;

import android.media.AudioRecord;
import android.os.AsyncTask;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;


class TextReceiver extends AsyncTask<String, Double, Integer> {
    private final String TAG = "RECEIVER";
    private Config config;
    private short[] buf;//buffer che contiene l'audio registrato
    private int numberOfRecordedSamples;//numero di campioni registrati
    private int pointer;//puntatore all'inizio del segnale utile, ovvero al segnale dopo la fine del preambolo e degli zeri
    private int modulatedStartingByteLength;
    private  byte[] demodulatedSignal;//codice demodulato fino alla fine della registrazione
    private byte[] demodulatedPayload; //codice utile demodulato
    private String decodedText;//messaggio decodificato
    private double[][] constellation;

    //variabili statiche
    static boolean stop;

    final static int ERROR_TOO_SHORT_SIGNAL = 1;
    final static int ERROR_PREAMBLE_NOT_FOUND = 2;
    final static int ERROR_ENDING_BYTE_NOT_FOUND = 3;
    final static int ERROR_INVALID_UTF8_ENCODING = 4;


    @Override
    protected void onPreExecute() {
        stop = false;
    }

    @Override
    protected Integer doInBackground(String... params) {
        //calcolo la minima dimensione del buffer interno di registrazione
        int minBufferSize = AudioRecord.getMinBufferSize(config.getSampleRate(), config.getChanInFormat(), config.getEncoding());
        Log.d(TAG,"MinBufferSize: "+minBufferSize);
        //inizializzo il registratore
        AudioRecord audioRecorder = new AudioRecord(
                config.getSourceId(),
                config.getSampleRate(),
                config.getChanInFormat(),
                config.getEncoding(),
                minBufferSize * 8);

        //alloco il buffer dove sarà immagazzinata la registrazione
        buf = new short[config.getBufferSize()];
        //inizio la registrazione
        audioRecorder.startRecording();
        //la registrazione continua fino a quando non viene premuto stop, o non si riempie il buffer di registrazione
        while ((!stop)&&(numberOfRecordedSamples < config.getBufferSize() - minBufferSize)) {
            int readSamples = audioRecorder.read(buf, numberOfRecordedSamples, minBufferSize);
            numberOfRecordedSamples += readSamples;
            //Log.d(TAG,"Numero campioni letti fino ad ora: "+ numberOfRecordedSamples);
        }
        Log.d(TAG,"Numero totale campioni letti: "+ numberOfRecordedSamples);
        audioRecorder.stop();
        audioRecorder.release();

        //istanzio il modem seguendo il parametro ricevuto
        String modulation = params[0];
        Modem modem = null;
        Log.d("allrecive","mod:"+modulation+" high: "+config.getHighFreq()+" low: "+config.getLowFreq()+" samplesperbit: "+config.getFskSamplesPerBit()+" samplerate: "+config.getSampleRate());
        if(modulation.equalsIgnoreCase("fsk")) modem = new Fsk(config.getHighFreq(),config.getLowFreq(),config.getMidFreq(),config.getFskSamplesPerBit(),config.getSampleRate());
        if(modulation.equalsIgnoreCase("bpsk")) modem = new Bpsk(config.getSampleRate(),config.getBpskCarrierFrequency(),config.getBpskPeriodsForSymbol());

        //genero il preambolo e la sequenza di zeri per poter effettuare la sincronizzazione
        short[] preamble = config.generatePreamble();
        short[] zero = config.generateZero();

        byte[] startingByte = new byte[]{config.getStartOfTransmissionByte()};
        short[] modulatedStartingByte = modem.modulate(startingByte,startingByte.length);
        modulatedStartingByteLength = modulatedStartingByte.length;
        if(numberOfRecordedSamples<preamble.length+zero.length){
            Log.d(TAG,"Segnale troppo corto");
            return ERROR_TOO_SHORT_SIGNAL;
        }
        //effettuo la sincronizzazione che ritorna il puntatore all'inizio del segnale utile, ovvero del segnale a meno del preambolo e della sequenza di zeri iniziale
        pointer = syncronization(preamble, buf, numberOfRecordedSamples) + preamble.length + zero.length;
        if ((pointer < 0) || (pointer >numberOfRecordedSamples)){
            Log.d(TAG,"Impossibile trovare Preambolo");
            return ERROR_PREAMBLE_NOT_FOUND;
        }
        demodulatedSignal = modem.demodulate(buf, numberOfRecordedSamples, pointer);
        int payloadLength = 0;
        boolean endingByteFound = false;
        for(int i = 0; i < demodulatedSignal.length; i++) {
            if (demodulatedSignal[i] == config.getEndOfTransmissionByte()){
                endingByteFound = true;
                payloadLength = i;
                break;
            }
        }
        if(!endingByteFound){
            Log.d(TAG,"impossibile trovare il byte finale");
            return ERROR_ENDING_BYTE_NOT_FOUND;
        }
        //carico il numero corretto di coordinate per la costellazione
        demodulatedPayload = Arrays.copyOfRange(demodulatedSignal,1, payloadLength);
        if(modem.getCostellation().length==1){
            constellation = new double[1][];
            constellation[0] = Arrays.copyOf(modem.getCostellation()[0],modem.getCostellation()[0].length);
        }
        if(modem.getCostellation().length==2){
            constellation = new double[2][];
            constellation[0] = Arrays.copyOf(modem.getCostellation()[0],payloadLength*8);
            constellation[1] = Arrays.copyOf(modem.getCostellation()[1],payloadLength*8);
        }
        try{
            decodedText = new String(demodulatedPayload, "UTF-8");
        }
        catch (UnsupportedEncodingException e){
            Log.d(TAG,"Impossibile decodificare UTF-8");
            return ERROR_INVALID_UTF8_ENCODING;

        }
        Log.d(TAG,"ricezione avvenuta correttamente");
        return 0;
    }
    public void setConfig(Config config){
        this.config = config;
    }
    public short[] getSignal(){
        short[] segnale = new short[numberOfRecordedSamples];
        System.arraycopy(buf,0,segnale,0, numberOfRecordedSamples);
        return segnale;
    }
    public int getPointer(){
        return pointer;
    }
    public byte[] getDemodulatedSignal(){
        return demodulatedSignal;
    }
    public byte[] getDemodulatedPayload(){
        return demodulatedPayload;
    }
    public String getDecodedText(){
        return decodedText;
    }
    public double[][] getConstellation(){
        return constellation;
    }
    /**
     * individua l'indice a cui inizia un preambolo all'interno di un segnale
     * @param preamble il preambolo da individuare
     * @param buffer buffer contentente il segnale
     * @param length lunghezza del segnale nel buffer
     * @return indice di inizio del preambolo
     */
    public int syncronization(short[] preamble, short[] buffer, int length){
        int offset = config.getSyncrnizationPrecision();
        if(length < preamble.length){
            Log.e(TAG, "syncronization: buffer più piccolo del preambolo. Verificare la condizione prima di chiamare il metodo.");
            throw new IllegalArgumentException();
        }
        double[] firstCorrelation = SignalProcessing.offsetCorrelation(preamble, buffer, length, 0, length, offset);
        int max = SignalProcessing.searchAbsoluteMax(firstCorrelation, firstCorrelation.length);
        int start = (max*offset)-offset;
        int stop = (max*offset)+offset;
        if(start < 0) start = 0;
        if(stop > length) stop = length;
        double[] secondCorrelation = SignalProcessing.offsetCorrelation(preamble,buffer, length, start, stop, 1);
        return SignalProcessing.searchAbsoluteMax(secondCorrelation,secondCorrelation.length)+((max*offset)-offset);
    }
}
