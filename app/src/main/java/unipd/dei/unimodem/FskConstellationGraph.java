package unipd.dei.unimodem;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

/**
 * Created by Sceriffo on 25/08/2017.
 */

public class FskConstellationGraph extends AppCompatActivity{
    private final String TAG = "FSK_GRAPH";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph_layout);
        Intent intent = getIntent();
        double[] dati = intent.getDoubleArrayExtra("frequenze");

        if(dati != null) {
            Config config = new Config(getApplicationContext());
            GraphView graph = (GraphView) findViewById(R.id.graph);

            graph.getViewport().setXAxisBoundsManual(true);
            graph.getViewport().setMinX(config.getLowFreq() - ((config.getHighFreq() - config.getLowFreq()) / 2));
            graph.getViewport().setMaxX(config.getHighFreq() + ((config.getHighFreq() - config.getLowFreq()) / 2));


            DataPoint[] datapoint = new DataPoint[dati.length];
            for (int i = 0; i < dati.length; i++) {
                datapoint[i] = new DataPoint(dati[i], 0);
            }
            PointsGraphSeries<DataPoint> serie2 = new PointsGraphSeries<>(datapoint);
            serie2.setSize(10);
            graph.addSeries(serie2);

            PointsGraphSeries<DataPoint> serie1 = new PointsGraphSeries<>(new DataPoint[]{
                    new DataPoint(config.getHighFreq(), 0),
                    new DataPoint(config.getLowFreq(), 0)
            });
            serie1.setSize(10);
            serie1.setColor(Color.GREEN);
            serie1.setShape(PointsGraphSeries.Shape.TRIANGLE);
            graph.addSeries(serie1);

            LineGraphSeries<DataPoint> line = new LineGraphSeries<>(new DataPoint[]{
                    new DataPoint(config.getMidFreq(), -1),
                    new DataPoint(config.getMidFreq(), 1),
            });
            line.setColor(Color.RED);
            graph.addSeries(line);

        }
        else{
            Log.e(TAG, "onCreate: ricevuto array null");
        }

    }
}