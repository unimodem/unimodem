package unipd.dei.unimodem;
import android.util.Log;

/**
 * Created by Sceriffo on 9/23/2017.
 */

public class Fsk implements Modem
{
    private double[] costellazione;
    private int highFreq;
    private int lowFreq;
    private int midFreq;
    private int samplesPerBit;
    private int sampleRate;

    public Fsk(int highFreq,int lowFreq, int midFreq, int samplesPerBit, int sampleRate){
        this.highFreq = highFreq;
        this.lowFreq = lowFreq;
        this.midFreq = midFreq;
        this.samplesPerBit = samplesPerBit;
        this. sampleRate = sampleRate;
    }

    public double[][] getCostellation(){
        double[][] risultato = new double[1][costellazione.length];
        System.arraycopy(costellazione,0,risultato[0],0,costellazione.length);
        return risultato;
    }

    /**
     * modulazione fsk
     * @param buffer dati da inviare
     * @param length lunghezza dati da inviare
     * @return buffer di dati modulati
     */
    public short[] modulate(byte[] buffer, int length) {
        short[] tmp = new short[length * samplesPerBit * 8];
        byte bit;//dove salvo il numero binario
        int freq;
        int pointer=0;
        for (int j = 0; j < length; j++) {
            bit = buffer[j];
            //Log.d("binary", Integer.toBinaryString(bit));
            for (int i = 0; i < 8; i++) {
                //vedo quali bit sono 1 e quali 0
                if ((bit & ((int) Math.pow(2, 7 - i))) != 0) {
                    freq = highFreq;
                    //Log.d("bit","1");
                } else {
                    freq = lowFreq;
                    //Log.d("bit","0");
                }
                System.arraycopy(SignalProcessing.sin(freq,samplesPerBit,sampleRate),0,tmp,pointer,samplesPerBit);
                pointer+=samplesPerBit;
            }
        }
        return tmp;
    }

    /**
     * demodulazione fsk
     * @param buffer dati ricevuti
     * @param length lunghezza dati ricevuti
     * @return buffer di dati demodulati
     */
    public byte[] demodulate(short[] buffer, int length, int pointer) {
        byte valore;
        short[] simbolo = new short[samplesPerBit];
        costellazione = new double[Math.round((length-pointer) / samplesPerBit) ];
        byte[] result = new byte[Math.round((length-pointer) / (8*samplesPerBit))];
        int pointBuf=pointer;
        int indexResult=0;
        Log.d("pointer: ",pointer+" ");
        for (int i = pointer; i < pointer + result.length; i++) {
            valore=0;
            for (int bitCounter = 0; bitCounter < 8; bitCounter++) {
                for (int j = 0; j < samplesPerBit; j++) simbolo[j] = buffer[pointBuf++];
                int freq = calcFrequencyZerocrossing(simbolo);
                costellazione[((i-pointer)*8)+bitCounter]=(double) freq; //frequenza simbolo i+bitCounter -esimo
                int state = determineState(freq);
                //if (state == -1);
                if (state == 1) valore += Math.pow(2, 7 - bitCounter);
            }
            result[indexResult++] = valore;

        }
        return result;
    }

    private double rootMeanSquared(short[] data) {
        double ms = 0;

        for (short aData : data) {
            ms += aData * aData;
        }

        ms /= data.length;
        return Math.sqrt(ms);
    }
    /**
     * determina lo stato del simbolo ovvero la frequenza del segnale nel periodo di simbolo
     * @param frequency frequenza rilevata
     * @return stato del simbolo (2 silenzio, 0 low frequency, 1 high frequency)
     */
    private int determineState(int frequency) {
        int state =-1;
        if (frequency < midFreq) state=0;
        else if(frequency >=midFreq) state=1;
        //Log.d("state"," "+state);
        return state;
    }

    /**
     * calcola la frequenza della porzione di segnale passata
     * @param data porzione di buffer passata
     * @return frequenza segnale passato
     */

    private int calcFrequencyZerocrossing(short[] data) {
        int numSamples = samplesPerBit;//data.length;
        int numCrossing = 0;

        for (int i = 0; i < numSamples-1; i++) {
            if ((data[i] > 0 && data[i+1] <= 0)
                    || (data[i] < 0 && data[i+1] >= 0)) {

                numCrossing++;
            }
        }

        double numSecondsRecorded = (double) numSamples
                / (double) sampleRate;
        double numCycles = numCrossing / 2;
        double frequency = numCycles / numSecondsRecorded;

        return (int) Math.round(frequency);
    }
}
