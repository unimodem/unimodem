package unipd.dei.unimodem;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

class Config {
    private SharedPreferences sharedPref;


    //PARAMETRI SPECIFICI NON MODIFICABILI DALL'UTENTE
    private final static int REQUEST_CODE = 1;//codice della richiesta dei permessi

    private final static byte endOfTransmissionByte = 0x4;//carattere EOT end of transmission
    private final static byte startOfTransmissionByte = 0x5;//carattere EOT end of transmission


    private final static int chanOutFormat = AudioFormat.CHANNEL_OUT_MONO;
    private final static int encoding = AudioFormat.ENCODING_PCM_16BIT;
    private final static int mode = AudioTrack.MODE_STATIC;
    private final static int streamType = AudioManager.STREAM_MUSIC;
    private final static int chanInFormat = AudioFormat.CHANNEL_IN_MONO;



    //costruttore
    Config(Context context){
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
    }



    int getSampleRate() {
        return Integer.parseInt(sharedPref.getString("sample_rate","11025"));
    }

    int getRecordingTimeoutSeconds() {
        return Integer.parseInt(sharedPref.getString("recording_timeout","10"));
    }

    int getBufferSize() {
        return getRecordingTimeoutSeconds()*getSampleRate();
    }


    int getNumberOfZeroSamples() {
        return (int)((Double.parseDouble(sharedPref.getString("zero_duration", "0.2")))*getSampleRate());
    }

    byte getEndOfTransmissionByte() {
        return endOfTransmissionByte;
    }
    byte getStartOfTransmissionByte() {
        return startOfTransmissionByte;
    }

    int getBpskCarrierFrequency() {
        return Integer.parseInt(sharedPref.getString("bpsk_frequency","3675"));
    }

    int getChanInFormat() {
        return chanInFormat;
    }

    int getChanOutFormat() {
        return chanOutFormat;
    }

    int getEncoding() {
        return encoding;
    }

    int getMidFreq() {
        return Math.round( ( getHighFreq() - getLowFreq() ) /2 )+ getLowFreq();
    }

    int getHighFreq() {
        return Integer.parseInt(sharedPref.getString("fsk_high_freq", "3675"));

    }

    int getLowFreq() {
        return Integer.parseInt(sharedPref.getString("fsk_low_freq", "2205"));
    }

    int getMode() {
        return mode;
    }

    int getBpskPeriodsForSymbol() {
        return Integer.parseInt(sharedPref.getString("bpsk_periods_for_symbol", "100"));
    }

    int getRequestCode() {
        return REQUEST_CODE;
    }

    int getFskSamplesPerBit() {
        return getSampleRate() / 49;
    }

    int getSourceId() {
        return Integer.parseInt(sharedPref.getString("audio_source", "6"));
    }

    int getStreamType() {
        return streamType;
    }

    int getSyncrnizationPrecision() {
        return Integer.parseInt(sharedPref.getString("syncronization_precision", "7"));
    }

    double getPreambleDuration() { return Double.parseDouble(sharedPref.getString("preamble_duration", "0.2"));}

    int getPreambleUpChirpFreqMin() { return Integer.parseInt(sharedPref.getString("preamble_up_chirp_freq_min", "200"));}

    int getPreambleUpChirpFreqMax() { return Integer.parseInt(sharedPref.getString("preamble_up_chirp_freq_max", "1575"));}

    int getPreambleSinFreq() { return Integer.parseInt(sharedPref.getString("preamble_sin_freq", "3675"));}

    short[] generatePreamble() {
        short[] result = new short[0];
                switch (Integer.parseInt(sharedPref.getString("preamble_shape", "0"))) {
            case 0:
                result = SignalProcessing.upChirp(getPreambleDuration(), getPreambleUpChirpFreqMin(), getPreambleUpChirpFreqMax(),getSampleRate());
            break;
            case 1:
                result = SignalProcessing.sin(getPreambleSinFreq(), (int)(getSampleRate()*getPreambleDuration()), getSampleRate());
            break;
        }
        return result;
    }

    short[] generateZero(){
        short[] zero = new short[getNumberOfZeroSamples()];
        for(int i = 0; i < getNumberOfZeroSamples(); i++)
            zero[i] = 0;
        return zero;
    }
}
