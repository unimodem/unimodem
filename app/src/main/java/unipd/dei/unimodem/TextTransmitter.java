package unipd.dei.unimodem;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.util.Log;


class TextTransmitter extends AsyncTask<String, Double, Void>{
    private final String TAG = "TEXT_TRANSMITTER";
    private short[] signal;
    private Config config;
    @Override
    protected Void doInBackground(String... params) {
        //il primo parametro contiene un array dei byte da inviare
        final byte[] buffer = params[0].getBytes();
        //il secondo parametro contiene la modulazione da utilizzare
        Modem modem = null;
        Log.d("alltrans","mod:"+params[1]+" high: "+config.getHighFreq()+" low: "+config.getLowFreq()+" samplesperbit: "+config.getFskSamplesPerBit()+" samplerate: "+config.getSampleRate());
        if(params[1].equalsIgnoreCase("fsk")) modem = new Fsk(config.getHighFreq(),config.getLowFreq(),config.getMidFreq(),config.getFskSamplesPerBit(),config.getSampleRate());
        if(params[1].equalsIgnoreCase("bpsk")) modem = new Bpsk(config.getSampleRate(),config.getBpskCarrierFrequency(),config.getBpskPeriodsForSymbol());

        //inserisco le sequenze necessarie alla sincronizzazione del segnale, e il carattere di fine trasmissione
        short[] preamble = config.generatePreamble();
        short[] zero = config.generateZero();
        byte[] startingByte = new byte[]{config.getStartOfTransmissionByte()};
        byte[] endingByte = new byte[]{config.getEndOfTransmissionByte()};

        //modulo il segnale e il carattere finale
        short[] modulatedStartingByte = modem.modulate(startingByte,startingByte.length);
        short[] modulatedSignal = modem.modulate(buffer, buffer.length);
        short[] modulatedEndingByte = modem.modulate(endingByte,endingByte.length);
        Log.d(TAG,"Modulazioni Effettuate");
        //concateno i pezzi del segnale da inviare
        signal = SignalProcessing.concat(preamble,zero,modulatedStartingByte,modulatedSignal,modulatedEndingByte);

        //genero il riproduttore audio
        AudioTrack audioPlayer = new AudioTrack(
                config.getStreamType(),
                config.getSampleRate(),
                config.getChanOutFormat(),
                config.getEncoding(),
                signal.length * 2,
                config.getMode()
        );
        audioPlayer.write(signal, 0, signal.length);
        audioPlayer.play();
        return null;
    }

    public short[] getSignal(){
        return signal;
    }
    public void setConfig(Config config){
        this.config = config;
    }
}
