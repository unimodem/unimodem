package unipd.dei.unimodem;

interface Modem {

    short[] modulate(byte[] buffer, int length);

    byte[] demodulate(short[] buffer, int length, int pointer);

    double[][] getCostellation();

}