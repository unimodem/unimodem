package unipd.dei.unimodem;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class TransmitFragment extends Fragment {

    final static String TAG = "TRANSMIT_FRAGMENT";
    private String modulation;
    private TextTransmitter textTransmitter;
    public TransmitFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transmit, container, false);

        Button receiveButton = (Button) view.findViewById(R.id.transmitButton);
        receiveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                transmit(v);
            }
        });
        Button modulatedSignalGraphButton = (Button) view.findViewById(R.id.modulated_graph_button);
        modulatedSignalGraphButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent graph = new Intent(getActivity().getApplicationContext(), TransmittedSignalGraph.class);
                graph.putExtra("signal", textTransmitter.getSignal());
                startActivity(graph);
            }
        });
        modulatedSignalGraphButton.setEnabled(false);
        return view;
    }

    public void transmit(View view) {
        Log.d(TAG,"chiamata al metodo transmit");
        //recupera il testo inserito dall'utente e la modulazione scelta. Se non nulle inizia la procedura di trasmissione.
        EditText editText = (EditText) getView().findViewById(R.id.editText);

        String inputText = editText.getText().toString();
        RadioGroup radioModulation = (RadioGroup)  getView().findViewById(R.id.radioModulation);
        int idModulation = radioModulation.getCheckedRadioButtonId();
        if(!inputText.isEmpty() && idModulation!=-1){
            //inizializzo un nuovo trasmettitore, impostando le azioni da compiere dopo la trasmissione
            textTransmitter = new TextTransmitter() {
                @Override
                protected void onPostExecute(Void aVoid) {
                    Log.d(TAG,"Trasmissione Finita");
                    getView().findViewById(R.id.modulated_graph_button).setEnabled(true);
                    getView().findViewById(R.id.transmitButton).setEnabled(true);
                }
                @Override
                protected void onPreExecute(){
                    this.setConfig(new Config(getContext()));
                }
            };

            //assegno al parametro modulazione la modulazione scelta dall'utente.
            switch (radioModulation.getCheckedRadioButtonId()) {
                case R.id.fsk:
                    modulation = "fsk";
                    break;
                case R.id.bpsk:
                    modulation = "bpsk";
                    break;
            }

            //disattivo i pulsanti
            toast("Trasmetto");
            //grafico.setEnabled(false);
            getView().findViewById(R.id.transmitButton).setEnabled(false);

            Log.d(TAG,"avvio esecuzione asincrona");
            //avvio la trasmissione
            textTransmitter.execute(inputText, modulation);
        }
        else {
            toast("Inserire del testo e scegliere una modulazione");
        }
    }

    public void toast(String text){
        Toast.makeText(getActivity().getApplicationContext(),text,Toast.LENGTH_SHORT).show();
    }
}
