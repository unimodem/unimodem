package unipd.dei.unimodem;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;


public class ReceivedSignalGraph extends AppCompatActivity {
    private final static String TAG = "RECEIVE_SIGNAL_GRAPH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graphsignal_layout);

        Intent intent = getIntent();
        short[] signalPoints = intent.getShortArrayExtra("signal");
        int pointer = intent.getIntExtra("pointer", 0);

        if(signalPoints!=null) {
            GraphView graph = (GraphView) findViewById(R.id.signalgraph);

            graph.getViewport().setScrollableY(true);
            graph.getViewport().setYAxisBoundsManual(true);
            int mean = SignalProcessing.getMean(signalPoints,signalPoints.length);
            //imposto la dimensione Y a 5 volte quella della media
            graph.getViewport().setMinY(-mean*5);
            graph.getViewport().setMaxY(mean*5);

            graph.getViewport().setScrollable(true);
            graph.getViewport().setXAxisBoundsManual(true);
            graph.getViewport().setScalable(true);
            graph.getViewport().setMinX(0);
            graph.getViewport().setMaxX(signalPoints.length);

            DataPoint[] datapoint = new DataPoint[signalPoints.length];
            for (int i = 0; i < signalPoints.length; i++) {
                datapoint[i] = new DataPoint(i, signalPoints[i]);
            }
            LineGraphSeries<DataPoint> signal = new LineGraphSeries<>(datapoint);
            graph.addSeries(signal);

            if(pointer!=0){
                PointsGraphSeries<DataPoint> pointerIndicator = new PointsGraphSeries<>(new DataPoint[]{
                        new DataPoint(pointer, 0)
                });
                pointerIndicator.setSize(20);
                pointerIndicator.setColor(Color.GREEN);
                pointerIndicator.setShape(PointsGraphSeries.Shape.TRIANGLE);
                graph.addSeries(pointerIndicator);
            }
        }
        else{
            Log.e(TAG, "onCreate: ricevuto array null");
        }
    }
}