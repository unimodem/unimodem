package unipd.dei.unimodem;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
    }


    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        Log.d("preference",preference.getKey());
        if(preference.getKey().equals("ripristina")){
            SharedPreferences shpreference=PreferenceManager.getDefaultSharedPreferences(getActivity());
            shpreference.edit().clear().commit();
            toast("ripristino impostazioni di default...");
            getActivity().onBackPressed();
        }
        return true;
    }

    private void toast(String text){
        Toast.makeText(getActivity().getApplicationContext(),text, Toast.LENGTH_SHORT).show();
    }
}

