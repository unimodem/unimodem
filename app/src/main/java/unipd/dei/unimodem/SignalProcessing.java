package unipd.dei.unimodem;
import android.util.Log;

/*
 * CLASSE CONTENTENTE METODI STATICI PER L'ELABOTAZIONE DI SEGNALI
 */
class SignalProcessing {
    private static final String TAG = "SIGNAL_PROCESSING";

    /**
     * simula una funzione seno discreta di lunghezza finita
     * @param freq frequenza del seno
     * @param length lunghezza in campioni del seno
     * @param sampleRate frequenza di campionamento della funzione
     * @return funzione seno
     */
    public static short[] sin(int freq, int length, int sampleRate) {
        short[] tmp = new short[length];
        int pointer = 0;
        for (int y = 0; y < length; y++)
            tmp[pointer++] = ((short) (Short.MAX_VALUE * Math.sin((2 * Math.PI) * ((y *1.0f) / sampleRate) * freq)));//adatto l'amipiezza del seno all'ampiezza massima degli short
        return tmp;
    }


    public static short[] cos(int freq, int length, int sampleRate) {
        short[] tmp = new short[length];
        int pointer = 0;
        for (int y = 0; y < length; y++)
            tmp[pointer++] = ((short) (Short.MAX_VALUE * Math.cos((2 * Math.PI) * ((y *1.0f) / sampleRate) * freq)));//adatto l'amipiezza del seno all'ampiezza massima degli short
        return tmp;
    }

    /**
     * Converte il byte passato come parametro in una sequenza di bit restituita sotto forma di array di boolean
     * @param b byte di cui individuare i bit
     * @return array di boolean contentente la rappresentazione del byte
     */
    public static boolean[] convertToBits(byte b) {
        boolean[] bits = new boolean[8];
        for (int i = 0; i < bits.length; i++) {
            bits[7 - i] = ((b & (1 << i)) != 0);
        }
        return bits;
    }



    /**
     * concatena in sequenza in un unico array una serie di array di short passati come parametri
     * @param params array da concatenare
     * @return array risultato dalla concatenazione
     */
    public static short[] concat(short[]...params) {
        int length = 0;
        for(short[] array : params){
            length += array.length;
        }
        short[] result = new short[length];
        int position = 0;
        for(int i = 0; i < params.length; i++){
            System.arraycopy(params[i], 0, result, position, params[i].length);
            position += params[i].length;
        }
        return result;
    }

    /**
     * costruisce un segnale up-chirp, ovvere un seno la sui frequenza aumenta all'aumentare del tempo
     * @param duration durata del segnale
     * @param freqMax frequenza massima da raggiungere
     * @param freqMin frequenza minima da raggiungere
     * @param sampleRate tasso di campionamento
     * @return
     */
    public static short[] upChirp(double duration, int freqMin, int freqMax, int sampleRate) {
        short[] tmp = new short[(int)(duration*sampleRate)];
        double slope = (double)(freqMax-freqMin)/(duration);//pendenza dell'up-chirp
        Log.d(TAG, "upChirp: slope = "+slope);
        for (int n = 0; n < tmp.length; n++){
            tmp[n] = (short) (Short.MAX_VALUE * Math.sin((2*Math.PI)*((freqMin * ((double)n)/sampleRate) + slope*Math.pow((((double)n)/sampleRate),2))));
            //Log.d(TAG, "upChirp: tmp"+tmp[n]);
        }
            return tmp;
    }

    /**
     * Calcola la cross-correlazione tra il segnale conosciuto e il segnale ricevuto calcolata solamente in alcuni punti, scostando il preambolo rispetto al buffer di un step indicato come parametro
     * @param preamble preambolo da trovare nel buffer di ricezione
     * @param buffer segnale ricevuto
     * @param length lunghezza dati ricevuti
     * @param start indice del buffer da cui iniziare a calcolare la correlazione
     * @param stop indice del buffer fino al quale calcolare la correlazione
     * @param step passo con cui va calcolata la correlazione
     * @return crossCorrelazione dei segnali
     */
    public static double[] offsetCorrelation(short[] preamble, short[] buffer, int length, int start, int stop, int step){
        //verifico delle condizioni iniziali per ottimizzare il calcolo della cross correlazione in casi non utili
        if( (stop < start) || (stop > length)) {
            throw new IllegalArgumentException("offsetCorrelation: Verificare la correttezza dei parametri");
        }
        Log.d(TAG, "calcolo della correlazione tra "+start+" e "+stop);
        double[] correlation = new double[(int)Math.ceil(((double)stop - (double)start) / (double)step)];
        int tmp = 0;
        //la correlazione viene calcolata solamente in alcuni punti, scostando il preambolo rispetto al buffer di un step indicato come parametro
        for(int i = start; i < stop; i += step){
            correlation[tmp] = 0;
            for(int j = 0; j < preamble.length; j++){
                if(i+j<length) {
                    correlation[tmp] += preamble[j] * buffer[j + i];
                }
            }
            tmp++;
        }
        return correlation;
    }

    /**
     * calcola l'indice a cui si trova il massimo del segnale passato come array di double
     * @param segnale di cui trovare il massimo
     * @param length lunghezza del segnale
     * @return indice a cui si trova il massimo valore del segnale
     */
    public static int searchAbsoluteMax(double[] segnale, int length){
        double max = 0;
        int index = 0;
        for(int i = 0; i < length; i++){
            if(Math.abs(segnale[i]) > max) {
                max = Math.abs(segnale[i]);
                index = i;
            }
        }
        return index;
    }
    public static int getMean(short[] segnale, int length) {
        double media = 0;
        for (double i : segnale) {
            media += Math.abs(i);
        }
        media /= length;
        return (int) Math.floor(media);
    }
}