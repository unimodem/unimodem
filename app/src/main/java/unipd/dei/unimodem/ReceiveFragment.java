package unipd.dei.unimodem;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class ReceiveFragment extends Fragment {
    private final static String TAG = "RECEIVE_FRAGMENT";
    private static TextReceiver textReceiver;
    private String modulation;

    public ReceiveFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_receive, container, false);

        Button receiveButton = (Button) view.findViewById(R.id.receiveButton);
        receiveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                receive(v);
            }
        });

        Button stopButton = (Button) view.findViewById(R.id.stopButton);
        stopButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                stop(v);
            }
        });

        Button constellationGraphButton = (Button) view.findViewById(R.id.constellation_graph_button);
        constellationGraphButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //a ciascuna modulazione la sua possibilità di implementare il grafico in modo specifico
                if(modulation.equalsIgnoreCase("fsk")) {
                    Intent graph = new Intent(getActivity().getApplicationContext(), FskConstellationGraph.class);
                    double[][] constellationValues = textReceiver.getConstellation();
                    if(constellationValues[0] != null){
                        graph.putExtra("frequenze",constellationValues[0]);
                    }
                    startActivity(graph);

                } else if(modulation.equalsIgnoreCase("bpsk")) {
                    Intent graph = new Intent(getActivity().getApplicationContext(), BpskConstellationGraph.class);
                    double[][] constellationValues = textReceiver.getConstellation();
                    if(constellationValues[0] != null){
                        graph.putExtra("constellationSin",constellationValues[0]);
                    }
                    if(constellationValues[1] != null){
                        graph.putExtra("constellationCos",constellationValues[1]);
                    }
                    startActivity(graph);
                }
            }
        });

        Button signalGraphButton = (Button) view.findViewById(R.id.signal_graph_button);
        signalGraphButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent graph = new Intent(getActivity().getApplicationContext(), ReceivedSignalGraph.class);
                graph.putExtra("signal", textReceiver.getSignal());
                graph.putExtra("pointer", textReceiver.getPointer());
                startActivity(graph);
            }
        });

        signalGraphButton.setEnabled(false);
        constellationGraphButton.setEnabled(false);
        return view;
    }

    public void receive(View view) {
        Log.d(TAG,"chiamata al metodo receive");
        //verifico che sia stata scelta una modulazione e inizio la ricezione
        RadioGroup radioModulation = (RadioGroup)  getView().findViewById(R.id.radioModulation);
        int idModulation = radioModulation.getCheckedRadioButtonId();
        if(idModulation!=-1){
            //inizializzo un nuovo ricevitore, impostando le azioni da compiere dopo la trasmissione
            textReceiver = new TextReceiver() {
                @Override
                protected void onPostExecute(Integer errore) {
                    getView().findViewById(R.id.receiveButton).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.stopButton).setVisibility(View.INVISIBLE);
                    getView().findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
                    switch(errore){
                        case 0:
                            TextView receivedText = (TextView) getView().findViewById(R.id.receivedText);
                            receivedText.setVisibility(View.VISIBLE);
                            receivedText.setText(textReceiver.getDecodedText());
                            toast("Ricezione Avvenuta Correttamente");
                            getView().findViewById(R.id.constellation_graph_button).setEnabled(true);
                            break;
                        case TextReceiver.ERROR_TOO_SHORT_SIGNAL:
                            setControlText("Segnale Troppo Corto");
                            break;
                        case TextReceiver.ERROR_PREAMBLE_NOT_FOUND:
                            setControlText("Impossibile trovare preambolo");
                            break;
                        case TextReceiver.ERROR_ENDING_BYTE_NOT_FOUND:
                            setControlText("impossibile trovare il byte finale");
                            break;
                        case TextReceiver.ERROR_INVALID_UTF8_ENCODING:
                            setControlText("Impossibile decodificare in UTF-8");
                            getView().findViewById(R.id.constellation_graph_button).setEnabled(true);
                            break;
                    }
                    getView().findViewById(R.id.signal_graph_button).setEnabled(true);
                    getView().findViewById(R.id.receiveButton).setEnabled(true);
                }
                @Override
                protected void onPreExecute(){
                    this.setConfig(new Config(getContext()));
                    getView().findViewById(R.id.controlText).setVisibility(View.INVISIBLE);
                    getView().findViewById(R.id.signal_graph_button).setEnabled(false);
                    getView().findViewById(R.id.constellation_graph_button).setEnabled(false);
                    getView().findViewById(R.id.receiveButton).setVisibility(View.INVISIBLE);
                    getView().findViewById(R.id.stopButton).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.receivedText).setVisibility(View.INVISIBLE);
                    TextReceiver.stop = false;
                    getView().findViewById(R.id.progressBar).setVisibility(View.VISIBLE);

                }
            };

            //assegno al parametro modulazione la modulazione scelta dall'utente.
            switch (radioModulation.getCheckedRadioButtonId()) {
                case R.id.fsk:
                    modulation = "fsk";
                    break;
                case R.id.bpsk:
                    modulation = "bpsk";
                    break;
            }

            //disattivo i pulsanti
            getView().findViewById(R.id.receiveButton).setEnabled(false);

            Log.d(TAG,"avvio esecuzione asincrona");
            //avvio la trasmissione
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                textReceiver.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, modulation);
            else textReceiver.execute(modulation);
        }
        else {
            toast("Scegliere una modulazione");
        }
    }

    public void stop(View view) {
        Log.d(TAG,"chiamata a stop");
        if(!TextReceiver.stop){
            TextReceiver.stop = true;
            toast("stop");
        }
    }


    private void toast(String text){
        Toast.makeText(getActivity().getApplicationContext(),text, Toast.LENGTH_SHORT).show();
    }
    private void setControlText(String text){
        TextView controlText = (TextView) getView().findViewById(R.id.controlText);
        controlText.setVisibility(View.VISIBLE);
        controlText.setText(text);
    }
}