package unipd.dei.unimodem;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final static String TAG = "MAIN_ACTIVITY";
    private Config config;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        //carico le impostazioni
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        config = new Config(this.getApplicationContext());

        Log.d(TAG,"Verifica dei permessi");
        //verifica a runtime dei permessi dell'applicazione
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, config.getRequestCode());
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        IntroFragment fragment = new IntroFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_intro) {
            IntroFragment introFragment = new IntroFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, introFragment).addToBackStack(null).commit();
        } else if (id == R.id.nav_transimt) {
            TransmitFragment transmitFragment = new TransmitFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, transmitFragment).addToBackStack(null).commit();
        } else if (id == R.id.nav_receive) {
            ReceiveFragment receiveFragment = new ReceiveFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, receiveFragment).addToBackStack(null).commit();
        } else if (id == R.id.nav_manage) {
            SettingsFragment settingsFragment = new SettingsFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, settingsFragment).addToBackStack(null).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(requestCode == config.getRequestCode()){
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permessi concessi
                Log.d(TAG, "onRequestPermissionsResult: i permessi sono stati concessi");
            } else {
                //permessi negati, vengono richiesti nuovamente
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, config.getRequestCode());
            }
        }
    }

}